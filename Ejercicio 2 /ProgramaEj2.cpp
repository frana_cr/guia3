// Se llaman las librerias a implementar
#include <stdlib.h>
#include <iostream>

using namespace std;
// Se llama al archivo que contiene las funciones que crean el programa
#include "Listas.h"

// Se crea el main donde se llevan a cabo la funcionalidad del programa
int main(void){
	//Se crea la variable de tipo Lista para el objeto lista1
	Lista lista1;
	//Se crea la variable de tipo Lista para el objeto lista2
	Lista lista2;
	//Se crea la variable de tipo Lista para el objeto lista3
	Lista lista3;
	// Se crea esta variable para almacenar la opcion
	int opcion;
	// Se crea para almacenar valores a las listas
	int numero_ingresado;

	cout << "CREANDO LISTAS " << endl;
// La opcion comenzara siendo igual a 1
	opcion = 1;
	// Se recorre el ciclo while cuando la opcion sea igual a 1, se almacenaran valores a la primera lista
	while(opcion == 1){
		// Se solicitan los numero de la primera lista
		cout << "INGRESE LOS NUMEROS DE LA PRIMERA LISTA: ";
		// Se captura el numero
		cin >> numero_ingresado;
		// Se imprime un salto de linea
		cout << endl;
		// Se ejecuta el metodo para incorporar el nuevo numero de manera ordenada
		lista1.lista_orden(numero_ingresado);
		// Se ejecuta el metodo para mostrar lista por pantalla
		cout << "PRIMERA LISTA" << endl;
		lista1.lista_imprimir();
		cout << endl;
		// Se consulta al usuario si desea opcion
		cout << "Ingrese 1: PARA SEGUIR INCORPORANDO NUMEROS, 2: PARA SEGUIR CON LA OTRA LISTA ";
		// Se almacena la respuesta en opcion
		cin >> opcion;
	}

	// Si la opcion es igual a 2, se recorrera el ciclo while, y se almacenaran valores a la segunda lista
	while(opcion == 2){
		// Se solicita el numero para la segunda lista
		cout << "INGRESE LOS NUMEROS PARA LA SEGUNDA LISTA: ";
		// Se captura el numero
		cin >> numero_ingresado;
		// Se imprime un salto de linea
		cout << endl;
		// Se ejecuta un metodo para incorporar el nuevo numero y ordenarlo
		lista2.lista_orden(numero_ingresado);
		// Por medio de esta funcion se muestra la lista en pantalla
		cout << "SEGUNDA LISTA" << endl;
		lista2.lista_imprimir();
		cout << endl;
		// Se consulta al usuario si desea opcion
		cout << "Ingrese 2:PARA SEGUIR INCORPORANDO NUMEROS, 3: PARA VER LA LISTA FINAL: ";
		// Se almacena la respuesta en opcion
		cin >> opcion;
	}

// Si la opcion es igual a 3, signifca que el usuario quiere ver las listas
	if(opcion == 3){
	cout << endl;
	// Se imprime que se estan mezclando las listas
	cout << "MEZCLANDO LAS LISTAS" << endl;

	/* Para llenar la tercera lista, se leeran todos los elementos de la lista 1 y se iran agregando a la lista 3
		luego se hara lo mismo con los elementos de la lista 2. */

	// Se lee la lista 1 y se compara con el inicio
	Nodo* auxiliar = lista1.Lista_inicio();
	// Se recorre un ciclo que cuando el siguiente es diferente de vacio
	while(auxiliar->siguiente != NULL){
		// Se ordenan los numeros en base a la lista
		lista3.lista_orden(auxiliar->valor);
		auxiliar=auxiliar->siguiente;
	}
	lista3.lista_orden(auxiliar->valor);

	// Se lee la lista 2 y se compara con la del inicio con el nodo auxiliar
	Nodo* auxiliar2 = lista2.Lista_inicio();
		// Se recorre el ciclo para ver si los valores siguientes del auxiliar son diferente de vacio
	while(auxiliar2->siguiente != NULL){
		// Se ordenan los numeros en base a la lista del auxiliar que contiene el numero
		lista3.lista_orden(auxiliar2->valor);
		auxiliar2=auxiliar2->siguiente;
	}
	// La lista tres es ordenada
	lista3.lista_orden(auxiliar2->valor);
	// Se imprime la primera lista
	cout << " LA PRIMERA LISTA ES:" << endl;
	lista1.lista_imprimir();
	cout << endl;
	// Se imprime la segunda lista
	cout << " LA SEGUNDA LISTA ES:" << endl;
	lista2.lista_imprimir();
	cout << endl;
	// Se imprime la mezcla y tercera lista
	cout << "LA TERCERA LISTA ES:" << endl;
	//se imprime en pantalla la mezcla de ambas listas
	lista3.lista_imprimir();
	}
}