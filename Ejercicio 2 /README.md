#  Guia 3 - EJERCICIO 2-

# Comenzando
En el desarrollo de la primer ejercicio de la tercera guia se debe realizar con el lenguaje C++, implementando clases para la construccion de este en conjunto con su respectivo archivo de cabecera y Makefile para la compilacion y ejecucion del programa. En la actividad se debe utilizar las listas enlazadas simples, la idea de este ejercicio es que se pueda construir un programa que genere dos listas enlazadas ordenadas estas deben leer los datos y formar una tercera lista la cual tiene que contener la mezcla de los elementos de ambas listas, ademas se debe mostrar el contenido de estas listas al usuario. 

## ---- COMO SE DESARROLLO ----
El programa consta de archivos principales los cuales permiten su principal funcionamiento y que el programa no tenga problemas para su desarrollo.

- El primer archivo es Nodo.h este archivo contiene los atributos y metodos que permiten la construcccion del nodo para que este funcione y se puedan conformar las listas, tambien se llama a las librerias, hay ciertos atributos que son privados en este caso se declaran de igual manera pero no hay atributos de este tipo, tambien los atributos publicos contienen el constructor del nodo donde tambien estan las variables del valor, el nodo siguiente y tambien la funcion que se encarga de crear el nuevo nodo esta funcion se declara de ante mano con un caracter int para que luego el valor igresado tome el valor por defecto del nodo y no tenga problemas para pasar los parametros.

- El archivo Nodo.cpp contendra la libreria principal y tambien a su ve el archivo H que es el general de la clase el cual es la cabecera del programa, tambien se declara el constructor del objeto y se inicializan las variables, el valor del nodo creado sera igual a 0, tambien el puntero siguiente partira con el valor NULL. En la funcin Nodo_nuevo() se contendra principalmente al nodo inicial y tendra a su vez el valor del numero ingresado, tambien se declara que el valor sera igual al numero que ingrese el usuario y ademas el nodo con el puntero siguiente sera igual a vacio.

- Listas.h: Este archivo tendra ciertas librerias que ejecutan los programas principales sin mayores dificultades estas son:

    - fstream : Esta libreria permite el flujo de informacion desde y hacia ficheros.
    - iostream : Es la mas importante para el funcionamiento de los algoritmos.
    - stdlib.h : Permite contener los prototipos de las funciones y los tipos de estas para usos generales.
Este programa a su vez funciona de cabecera, contiene la clase y el objeto Lista, cotniene sus atributos privados y publicos, a su vez contiene las funciones principales que se encargan de poder interactuar con las listas y poder implementarlas en el programa principal. Estas funciones pueden ser para agregar el nodo al inicio y final de la lista, para ordenar los valores de la lista, para poder imprimir los valores que contenga la lista, tambien se muestra que el nodo debe devolver el puntero inicial y por ultimo una funcion de tipo booleana(TRUE/FALSE) la cual permitira saber si la lista se recorre de buena manera o no.

- El archivo Listas.cpp: Este archivo contendra las funciones que permiten el tratamiento de las listas, se llaman a sus librerias y a su vez al archivo cabecera de la clase Lista, se llama principalmente al constructor del con las declaraciones de que el inicio sera vacio y que a su vez tambien el numero de los elementos es igual a 0.

    - Funcion lista_agregar_inicio = Esta funcion permite agregar al inicio del nodo un elemento, esto se efectua principalmente si el primer nodo es diferente a vacio, si esta condicion se cumple el valor se puede agregar, si no, se tiene que recorrer a la sigueinte posicion para poder agregar el numero ingresado por el usuario. Finalmente el valor de los numeros ingresados aumentara en 1.

    - Funcion lista_agregar_final: Esta funcion permite que al nodo en su ultima posicion se pueda agregar el valor, se desarrolla de manera similar a la funcion mencionada con anterioridad, tambien verifica si hay algo antes de agregar y si la posicion es vacia, de esta manera se van agregando los valores que van al final de la lista.

    - lista_orden: Esta funcion permite que se ordenen los valores ingresados de la lista ordenando de mayor a menor segun el numero, tambien para esto hay una serie de condiciones las cuales permiten ver si los numeros que se agregan pueden ser ordenados. De esta forma se establecen ciertas condiciones tales como si no hay mas nodos se deja el nuevo nodo al inicio debido a que esta vacio, de modo contrario de esta forma tambien se puede ver si los numeros que se agregan se avance a la siguiente posicion si no esta vacio, para esto se pregunta por la posicion actual y la siguiente, de esta manera el puntero se agrega donde deberia estar y se agregan los nodos, tambien el valor de los elementos se aumenta en 1.  Esta funcion en general permite que si el usuario ingresa los numeros de manera desordenada estos luego se ordenen tal cual como se requiere en las listas ordenadas simples.


    - lista_imprimir: La funcion imprimir permite que al crear la lista esta luego sea impresa para que el usuario pueda visualizar los estados de la lista luego de agregar ciertos valores especificos. Tambien se recorren todos los elementos desde el inicio hasta el final de la lista, se decide tambien de que forma se separaran las impresiones de los valores de la lista, ademas se imprimen los valores de los numeros de manera ordenada.


   - Lista_inicio: Permite tambien crear el nodo que contrendra la lista del incio y tambien se devuelve el puntero al inicio de la lista. La funcionalidad radica en que permite poder mover el puntero cuando no esta en las posiciones iniciales al recorrer la lista.


    - lista_existe: Esta funcion se crea para poder ver si la lista existe, para esto se recorren los elementos de la lista por medio de un while que permite ver que si el auxiliar siguiente es diferente de vacio y a su vez se verifica si el valor ingresado es igual a otro para ver si se encuentra, si esto pasa se devuelve un valor TRUE lo que significa que si ha sido encontrado. Si esto no sucede el valor else pasara al numero siguiente buscando los valores. De otra manera fuera del While se verifica a su vez que el valor auxilar siguiente es igual al numero se retorna un valor TRUE porque fue encontrado de modo contrario se retorna un valor FALSE por lo que no ha encontrado dicho valor. Se realiza tambien de esta manera para comprobar recorriendo la funcion y de manera externa de que la lista si existe y contiene los valores que el usuario ingrese.

- El archivo ProgramaEj2: Este archivo es el que permite que funcione el programa en su totalidad, tambien permite que se usen los archivos creados anteriormente los cuales contiene las funciones principales que permiten la manipulacion de las listas y su uso continuo. Este programa logra funcionar en su totalidad sin mayores porblematicas, a su vez se describen comentarios sobre las acciones que se ejecutan en el codigo. 
Este archivo llama a las librerias tipicas para poder manipular las listas y los tipos de datos dentro de ellas, hay solo una funcion principal en donde se ubica el main, dentro de esta funcion se llaman a tres variables de tipo listas, estas luego seran importantes para almacenar los datos y luego poder mostrarlos en pantalla cuando se mezclen los numeros y den origen a una tercera lista. Tambien se declaran variables las cuales permiten almacenar la opcion y el numero ingresado por parte del usuario. 
Se recorre un ciclo while el cual permite que cuando el usuario ingrese 1 pueda ingresar los valores correspondientes a la primera lista, cuando el usuario desee no seguir mas ingresando valores, puede presionar la tecla 2 la cual de esta manera comenzara a preguntar los numeros que quiere que el usuario ingrese a la segunda lista, antes de imprimir y mostrar los datos numericos en pantalla estos se almacenan, se ordenan de manera creciente y finalmente se muestran en pantalla. Cuando el usuario presione el valor numerico 3 lo que se generera es que se imprima una tercera lista con los valores de las los listas creadas anteriormente. 
Estas opciones se mostraran inmediatamente luego de almacenar la variable del numero_ingresado. 

A u vez se genera un ciclo while con el nodo auxiliar, esto se realiza para llenar la tercera lista y de esta manera completarlos primero con la lista 1 y finalmente con la lista 2. En la lista tres los numeros son ordenados y finalmente se muestran en pantalla. El programa logra cumplir el objetivo de generar dos listas enlazadas y formar una tercera que resulte de la mezcla de ambas, se decidio que el usuario ingrese los numeros porque asi se puede ver el detalle de la funcionalidad y ademas se implementa un menu para que el usuario decida con mayor claridad hasta cuantos numeros desea ingresar en cada lista.Se logra mostrar finalmente el contenido de cada lista y el contenido final de la mezcla.

- La problematica del ejercicio radica en que se debe crear una lista enlazada ordenada en la cual se lean los datos de dos listas en la cual finalmente se forme una tercera lista con la mezcla de ambos elementos, esta mezcla se tiene que ir mostrando en pantalla al igual que las dos listas anteriores. 
# Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (vim o geany)

# Instalacion
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:
lsb_release -a (versión de Ubuntu)
En donde:
Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando.
Para descargar un editor de texto como vim se puede descargar de la siguiente manera:
sudo apt install vim
En donde: Por medio de este editor de texto se puede construir el codigo.
En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.

# Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa.

### INSTALaCION
Para poder llevar a cabo el funcionamiento del programa se requiere la instalacion del Make para esto primero hay que revisar que este en la computadora, para corroborar se debe ejecutar este comando:
- make.
De esta forma se puede ver si esta instalado para luego llevar a cabo el desarrollo del  programa, si este no esta instalado, se tiene que insertar el siguiente comando en la terminal de la carpeta del programa.
- sudo apt install make
De esta manera se procede a la creacion del make para que luego este se lleve a cabo y se pueda observar la construccion del archivo.
Luego de instalar el Make, para poder ejecutar las pruebas se tiene que en la terminal entrar en la carpeta donde se encuentran los archivos, luego ver si estan los archivos correctos mediante ls, finalmente se debe escribir make en la terminal.
Si ya se ha ejecutado con anterioridad un make, se tiene que escribir un make clean para borrar los archivos probados con anterioridad. Luego de ejecutar  en la terminal el comando:
- make
Debe mostrar los nombres de los archivos y el orden de estos con g++ y el nombre del archivo, aqui se puede visualizar si el programa no tiene problematicas. Si hay una problematica sale un mensaje y no salen todos los archivos con g++. Si se desarrollo completamente luego se puede ingresar el siguiente comando:
- ./ con el nombre del programa el cual contiene el main.
De esta manera se observa el funcionamiento del programa y de los demas archivos.

# Construido con
- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Geany: Editor de código.

# Versiones
## Versiones de herramientas:
- Ubuntu 20.04 LTS
- Geany 1.36-1build1
- Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/guia2-aed

# Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

# Expresiones de gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=730534&pageid=13732
A las Lecturas de internet y videos que me ayudaron a plantear una solucion posible: https://www.youtube.com/watch?v=hIpW1Djw1Qk
https://www.youtube.com/watch?v=NPmwuR4VbKk
https://www.youtube.com/watch?v=0NzAFk1CwaQ
https://es.ccm.net/faq/2842-la-lista-enlazada-simple
