// Se llaman las librerias a implementar (Se explican con mayor detalle en el Readme)
#include <fstream>
#include <iostream>
#include <stdlib.h>

using namespace std;
#ifndef LIST_H
#define LIST_H
// Se llaman a los archivos el cabecera.h y el que contiene las funciones .cpp
#include "Nodo.h"

// Se define la primera clase del objeto Lista
class Lista{
	// Se crean los atributos privados
	private:
		// Puntero que indicará cual es nodo de inicio de la lista
		Nodo *inicio;
		// Se crea el atributo que almacena la cantidad de elementos de la lista
		int numero_elementos;
	// Se define los atributos publicos
	public:
		// Se crea el constructor
		Lista();
		// Se crea la funcion que agrega el nodo inicial añadiendo numeros
		void lista_agregar_inicio(int numero);
		// Se crea la funcion que agrega el nodo final añadiendo numeros
		void lista_agregar_final(int numero);
		// Se crea la funcion que ordena los numeros de la lista
		void lista_orden(int numero);
		// Se crea la funcion que imprime la lista de numeros
		void lista_imprimir();
		// Se crea el nodo que devuelve el puntero
		Nodo* Lista_inicio();
		// Se crea la funcion que recorre la lista
		bool lista_existe(int numero);
};
#endif