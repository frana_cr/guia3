// Se llaman las librerias a implementar
#include <stdlib.h>
#include <iostream>

using namespace std;
// Se llama al archivo que contiene las funciones que crean el programa
#include "Listas.h"

// Se crea el main donde se llevan a cabo la funcionalidad del programa
int main(void){
	// Variable utilizada para la creacion de numeros ingresados
	int numero_ingresado;
	// Variable para almacenar el menor elemento de la lista
	int menor;
	// Variable para almacenar el mayor elemento de ls lista
	int mayor;
	// Variable que almacena las opciones
	int opcion;
	// Se almacena la variable del tipo Lista
	Lista lista;

	cout << "CREACION LISTAS" << endl;

	// La opcion comenzara siendo igual a 1
	opcion=1;
	// Se recorre el ciclo while cuando la opcion sea igual a 1, se almacenaran valores en la lista
	while(opcion == 1){
	// Se solicita el numero
		cout << "INGRESE UN NUMERO PARA AÑADIR A LA LISTA: ";
		// Se captura el numero
		cin >> numero_ingresado;
		// Imprime un salto de linea
		cout << endl;
		// Se ejecuta metodo para incorporar el nuevo numero y ordenarlo
		lista.lista_orden(numero_ingresado);
		// Se ejecuta el metodo para imprimir lista por pantalla
		cout << " LA LISTA NO CORRELATIVA " << endl;
		lista.lista_imprimir();
		cout << endl;
		// Se consulta al usuario si desea opcion
		cout << "INGRESE 1: PARA INCORPORAR OTRO NUMERO" << endl;
		cout << " SI DESEA SALIR Y VER LAS LISTAS INDIQUE OTRO CARACTER NUMERICO" << endl;
		// Se almacena la respuesta en opcion
		cin >> opcion;
	}
	// Se imprime que se completara la lista con los numeros que faltan
	cout << "COMPLETANDO LISTA CON NUMEROS FALTANTES" << endl;
	// El nodo axiliar pemitira agregar al inicio
	Nodo* auxiliar = lista.Lista_inicio();
	// Se deja menor un numero alto para ir comparando (Se escogio hasta el valor de 100)
	menor = 100;
	// Como se sabe que el menor valor de la lista es 1 se deja mayor en un numero menor
	mayor = 0;

	// Por medio de este ciclo se puede recorrer la lista cuando el valor de siguiente es diferente de Null
	while(auxiliar->siguiente != NULL){
		// En este if se consulta cuando el valor menos es mas grande que el valor del nodo
		if(menor > auxiliar->valor){
		// Si esto pasa "menor" toma el valor del nodo
			menor = auxiliar->valor;
		}
		// Si el mayor es el valor mas pequeño del nodo
		if(mayor < auxiliar->valor){
			// Si esto pasa "mayor" toma el valor del nodo
			mayor = auxiliar->valor;
		}
		// El auxiliar sera igualado al siguiente
		auxiliar = auxiliar->siguiente;
	}
	// Se hace la misma pregunta para el nodo que no alcanza a pasar por el ciclo While
	// Si el menor es el valor mas grande del nodo
	if(menor > auxiliar->valor){
			menor = auxiliar->valor;
		}
	// Si el valor mayor es el valor mas pequeño del nodo
	if(mayor < auxiliar->valor){
			mayor = auxiliar->valor;
		}

	// Ahora se hace un ciclo for para ir desde el numero "menor" hasta el numero "mayor"
	for(int i = menor; i <= mayor; i++){
		// Si la lista existe es diferente de vacio
		if(!lista.lista_existe(i)){
		// Por cada numero del ciclo se consulta si existe en la lista
			lista.lista_orden(i);

		}
	}
	// Se imprime finalmente la lista con todos los valores
	cout << "FINALMENTE LA LISTA ORDENADA DE MANERA CRECIENTE CON LOS VALORES ES :" << endl;
	lista.lista_imprimir();
	// Se imprime un salto de linea
	cout << endl;
}