// Se llaman las librerias a implementar
#include <iostream>
using namespace std;

#ifndef NODO_H
#define NODO_H
// Se crea la clase que contendra al objeto Nodo
class Nodo{
	// Se declaran los atributos privados
	private:
	// Se declaran los atributos de caracter publico
	public:
		// Se crea el constructor
		Nodo();
		// Se declara la variable que contiene el valor
		int valor;
		// Se crea el nodo que contiene el nodo siguiente
		Nodo* siguiente;
		// Se crea la funcion del nodo nuevo
		void Nodo_nuevo(int);
};
#endif