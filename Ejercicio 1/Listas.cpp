// Se llaman las librerias a implementar
#include <iostream>
using namespace std;

// Se llama al archivo de cabecera de la clase Lista
#include "Listas.h"
// Se llama al constructor
Lista::Lista(){
	// La variable inicio sera igual a vacio (null)
	inicio = NULL;
	// la variable numero sera igual a 0
	numero_elementos = 0;
}

// Se crea la funcion que agrega elementos al inicio que contiene al numero 
void Lista::lista_agregar_inicio(int numero){
	// Se crea el nuevo nodo
	Nodo *nodo_nuevo = new Nodo();
	// Se asigna el valor ingresado al nuevo nodo
	nodo_nuevo->Nodo_nuevo(numero);
	// Si el inicio es el primer nodo es diferente a vacio
	if(inicio != NULL){
		// El inicio sera igual al nodo nuevo
		inicio = nodo_nuevo;
	}else{
		// Si no es el primer elemento se modifican los punteros
		nodo_nuevo->siguiente = inicio;
		// El inicio sera igual al nuevo nodo
		inicio = nodo_nuevo;
	}
	// Aumenta en 1 la cantidad de elementos de la lista
	numero_elementos++;
}

// Esta funcion agrega elementos al final de la lista
void Lista::lista_agregar_final(int numero){
	// Se crea un nodo auxiliar que es igual al inicio
	Nodo *auxiliar = inicio;
	// Se crea un nodo nuevo que es igual al nuevo nodo
	Nodo *nodo_nuevo = new Nodo();
	// el nodo nuevo contendra al numero ingresado
	nodo_nuevo->Nodo_nuevo(numero);
	// Si el valor de inicio es diferente a vacio
	if(inicio != NULL){
		inicio = nodo_nuevo;
	}else{
		// Si no es el primer elemento recorre la lista hasta el final
		while(auxiliar->siguiente != NULL){
			auxiliar = auxiliar->siguiente;
		}
		// El ultimo nodo se deja apuntando al nuevo nodo ingresado
		auxiliar->siguiente = nodo_nuevo;
	}
	// Aumenta en 1 la cantidad de elementos de la lista
	numero_elementos++;
	}

// Se crea la funcion que ordena la lista con los valores que se ingresan 
void Lista::lista_orden(int numero_ingresado){
	// Se crea un nodo auxiliar que es igual al inicio
	Nodo *auxiliar = inicio;
	// Se crea un nodo nuevo que es igual al nuevo nodo
	Nodo *nodo_nuevo = new Nodo();
	// El nodo nuevo contendra al numero ingresado
	nodo_nuevo->Nodo_nuevo(numero_ingresado);

	// Si no hay mas nodos se deja el nuevo nodo al inicio, debido que el inicio es vacio
	if(inicio == NULL){
		// El inicio es igual al nodo nuevo
		inicio = nodo_nuevo;
	}else{
		// Si hay otros nodos se pregunta si el valor del Inicio es mayor
		if(inicio->valor > numero_ingresado){
			//  si esto ocurre el nuevo nodo pasa a ser el inicio
			nodo_nuevo->siguiente=inicio;
			inicio = nodo_nuevo;
		}else{
		// Como se agrega en orden antes de avanzar se pregunta por la posicion actual y la siguiente posicion
			while((auxiliar->siguiente != NULL) && (auxiliar->siguiente->valor < numero_ingresado)){
                auxiliar = auxiliar->siguiente;
            }
        // Cuando el puntero ya se ubico donde debería ir, el nodo se agrega
            nodo_nuevo->siguiente = auxiliar->siguiente;
            auxiliar->siguiente = nodo_nuevo;
			}
	}
	// Aumenta en 1 la cantidad de elementos de la lista
	numero_elementos++;
	}

// Se crea la funcion que imprime la lista
void Lista::lista_imprimir(){
	// 	Se recorren todos los elementos desde el inicio hasta el fin de la lista
	Nodo* auxiliar = inicio;
	while(auxiliar->siguiente != NULL){
		// Se imprime con esta separacion los valores de la lista
		cout << auxiliar->valor << " - ";
		auxiliar = auxiliar->siguiente;
	}
	// Se imprime el valor (auxiliar)
	cout << auxiliar->valor;
}

// Se crea el nodo que contiene la lista del inicio
Nodo* Lista::Lista_inicio(){
	// Devuelve el puntero al inicio de la lista
	return inicio;
}

// Se crea la funcion que permite ver que la lista existe
bool Lista::lista_existe(int numero){
	//	Se recorre todos los elementos de la lista
	Nodo* auxiliar = inicio;
	while(auxiliar->siguiente != NULL){
		// Se verifica si el valor ingresado ya se encuentra
		if(auxiliar->valor == numero){
			// Si se cumple se retorna un valor verdadero
			return true;
		}else{
			// Si no se encuentra pasa al numero siguiente
			auxiliar = auxiliar->siguiente;
		}
	}
	// Si el valor auxiliar del numero es igual al numero
	if(auxiliar->valor == numero){
		// Se retorna un true
		return true;
	}else{
		// De modo contrario se retorna un false
		return false;
	}
}