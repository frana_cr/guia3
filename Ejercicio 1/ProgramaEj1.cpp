// Se llaman las librerias a implementar
#include <iostream>

using namespace std;
// Se llama al archivo que contiene las funciones que crean el programa
#include "Listas.h"

// Se crea la funcion que permite mostrar las opciones al usuario
int opciones_menu(){
	// se solicita al usuario ingresar su opcion
    int opcion;
    // Se muestra la informacion en pantalla
    cout << "\n MENU OPCIONES LISTAS \n" << endl;
    // Se muestra el menu de opciones
	cout << "OPCION 1 - INGRESE NUMEROS" << endl;
	cout << "OPCION 0 - SALIR DEL PROGRAMA" << endl;

	// Se imprime para que el usuario ingrese su opcion y se captura el valor
	cout << "Ingrese su opcion:";
	cin >> opcion;
	return opcion;
}

// Se crea el main donde se llevan a cabo la funcionalidad del programa
int main(void){
	// Esta variable permite obtener el numero que ingrese el usuario
	int numero_ingresado;
	// Esta variable recoge la opcion del usuario
	int opcion;
	// Variable del tipo lista y el objeto
	Lista lista;

	// Se crea el ciclo while para mantener las opciones que seleccione el usuario
	while (opcion != 0){
		opcion = opciones_menu();
		switch(opcion){
			// Cuando el usuario INGRESE 1
			case 1:
			cout << "GENERANDO LISTA DE NUMEROS" << endl;
			// Se imprime que ingrese un numero a la lista
			cout << "INGRESE UN NUMERO PARA ANADIR A LA LISTA: " << endl;
			// Se captura el numero
			cin >> numero_ingresado;
			// Imprime un salto de linea
			cout << endl;
			// Se ejecuta metodo para incorporar el nuevo numero (nodo)
			lista.lista_orden(numero_ingresado);
			cout << "LA LISTA ES:" << endl;
			// Se ejecuta el metodo para mostrar lista por pantalla
			lista.lista_imprimir();
			cout << endl;
			break;
			// Se llaman a las opciones
			opcion = opciones_menu();

// CUANDO EL USUARIO INGRESE 0
case 0:
            // El programa se cierra
            cout << "CERRANDO PROGRAMA";
            exit(0);
        }
      }
        system("pause");
      return 0;
}
