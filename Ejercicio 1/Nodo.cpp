// Se incluye la libreria y el archivo r general de la clase
#include <iostream>
using namespace std;
#include "Nodo.h"

// Se crea el constructor del objeto y se inicializan las variables
Nodo::Nodo(){
	// El nodo creado tiene el valor 0	
	valor = 0;	
	// El nodo parte con el puntero siguiente y con el valor vacio(NULL)			
	siguiente = NULL;						
}

// Se crea la funcion que contendra el nodo nuevo y tendra el valor del numero ingresado
void Nodo::Nodo_nuevo(int numero_ingresado){	
	// El valor sera igual al numero ingresado en el nodo creado
	valor = numero_ingresado;
	siguiente = NULL;						
}